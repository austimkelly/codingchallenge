//
//  CodingChallengeTests.m
//  CodingChallengeTests
//
//  Created by Timothy Kelly on 8/29/12.
//  Copyright (c) 2012 Fizzy Artwerks. All rights reserved.
//

#import "CodingChallengeTests.h"
#import "MyNumberUtils.h"

@implementation CodingChallengeTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testPositiveIntegers
{
    
    NSNumber *one = [[NSNumber alloc] initWithInt:1];
    NSNumber *two = [[NSNumber alloc] initWithInt:2];
    NSNumber *three = [[NSNumber alloc] initWithInt:3];
    NSNumber *four = [[NSNumber alloc] initWithInt:4];
    NSNumber *five = [[NSNumber alloc] initWithInt:5];
    NSNumber *six = [[NSNumber alloc] initWithInt:6];
    NSNumber *seven = [[NSNumber alloc] initWithInt:7];
    
    NSSet *input = [[NSSet alloc] initWithObjects:seven, two, one, four, five, three, six, nil];
    NSArray *expected = [[NSArray alloc] initWithObjects:seven, six, five, four, nil];
    NSArray *actual = [MyNumberUtils findHighestNIntegers:input withCount:4];
    NSLog(@"--->testPositiveIntegers actual: %@", actual.description);
    STAssertEqualObjects(actual, expected, @"Arrays shold be equal for testPositiveIntegers");
    
}

- (void)testNegativeIntegers
{
    
    NSNumber *one = [[NSNumber alloc] initWithInt:-1];
    NSNumber *two = [[NSNumber alloc] initWithInt:-52];
    NSNumber *three = [[NSNumber alloc] initWithInt:-993];
    NSNumber *four = [[NSNumber alloc] initWithInt:-4000];
    NSNumber *five = [[NSNumber alloc] initWithInt:-5987];
    NSNumber *six = [[NSNumber alloc] initWithInt:-69987];
    NSNumber *seven = [[NSNumber alloc] initWithInt:-798765];
    
    NSSet *input = [[NSSet alloc] initWithObjects:seven, two, one, four, five, three, six, nil];
    NSArray *expected = [[NSArray alloc] initWithObjects:one, two, three, four, nil];
    NSArray *actual = [MyNumberUtils findHighestNIntegers:input withCount:4];
    NSLog(@"--->testPositiveIntegers actual: %@", actual.description);
    STAssertEqualObjects(actual, expected, @"Arrays shold be equal for testNegativeIntegers");
    
}



@end
