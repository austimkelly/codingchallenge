//
//  MyNumberUtils.h
//  CodingChallenge
//
//  Created by Timothy Kelly on 8/29/12.
//  Copyright (c) 2012 Tim Kelly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyNumberUtils : NSObject


// Find the highest N integers from a set of unordered integers. This method operates on the presumption that all
// members of the set are an NSNumber and will be converted to primitive integer types accordingly.
//
// Parameters:
//      setOfIntegers: The set of integers to scan, presumed to be of type NSNumber. 
//      count: An unsigned integer for the number of highest integers to return
//
//  Return
//      An array of integers, sorted in descending order. The full set of sorted integers is returned is count is > the size of setOfIntegers
//
//
+ (NSArray *)findHighestNIntegers:(NSSet *)setOfNSIntegers withCount:(NSUInteger)count;


@end
