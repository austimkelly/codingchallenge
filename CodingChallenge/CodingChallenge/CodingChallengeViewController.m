//
//  CodingChallengeViewController.m
//  CodingChallenge
//
//  Created by Timothy Kelly on 8/29/12.
//  Copyright (c) 2012 Fizzy Artwerks. All rights reserved.
//

#import "CodingChallengeViewController.h"
#import "MyNumberUtils.h"

@interface CodingChallengeViewController ()

@property (weak, nonatomic) IBOutlet UILabel *testNumbersLabel;
@property (weak, nonatomic) IBOutlet UILabel *testResultLabel;

@end

@implementation CodingChallengeViewController
@synthesize testNumbersLabel;
@synthesize testResultLabel;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setTestNumbersLabel:nil];
    [self setTestResultLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSSet *)generateNRandomNumbers:(NSUInteger)count {
    
    NSMutableSet *returnSet = [[NSMutableSet alloc] init];
    while (returnSet.count < count) {
        // Generate a random number between -10,000 and 10,000
        int newRand = rand() % 20000;
        newRand -= 10000;
    
        NSNumber *newVal = [[NSNumber alloc] initWithInt:newRand];
        
        if (![returnSet containsObject:newVal]){
            [returnSet addObject:newVal];
        }
    }
    
    return returnSet;
    
}


- (IBAction)doTest:(id)sender {
    
    NSSet *testSet = [self generateNRandomNumbers:10];
    self.testNumbersLabel.text = [[testSet allObjects] componentsJoinedByString:@", "];
    
    NSArray *result = [MyNumberUtils findHighestNIntegers:testSet withCount:4];
    self.testResultLabel.text = [result componentsJoinedByString:@", "];
    
    
}


@end
