//
//  CodingChallengeAppDelegate.h
//  CodingChallenge
//
//  Created by Timothy Kelly on 8/29/12.
//  Copyright (c) 2012 Fizzy Artwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CodingChallengeAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
