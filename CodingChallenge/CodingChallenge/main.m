//
//  main.m
//  CodingChallenge
//
//  Created by Timothy Kelly on 8/29/12.
//  Copyright (c) 2012 Fizzy Artwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CodingChallengeAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CodingChallengeAppDelegate class]));
    }
}
