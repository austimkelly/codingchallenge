//
//  MyNumberUtils.m
//  CodingChallenge
//
//  Created by Timothy Kelly on 8/29/12.
//  Copyright (c) 2012 Fizzy Artwerks. All rights reserved.
//

#import "MyNumberUtils.h"

@implementation MyNumberUtils


+ (NSArray *)findHighestNIntegers:(NSSet *)setOfNSIntegers withCount:(NSUInteger)count{
    
    if (count > setOfNSIntegers.count){
        return [setOfNSIntegers allObjects];
    }

    NSArray *sortedList = [[setOfNSIntegers allObjects] sortedArrayUsingComparator:^(id a, id b){
        
        NSInteger aInt = [a intValue];
        NSInteger bInt = [b intValue];
        if ( aInt > bInt ) {
            return (NSComparisonResult)NSOrderedAscending;
        } else if ( aInt < bInt ) {
            return (NSComparisonResult)NSOrderedDescending;
        } else {
            return (NSComparisonResult)NSOrderedSame;
        }

    }];
    
    NSRange range = NSMakeRange(0, count);
    return [sortedList subarrayWithRange:range];
    
}

@end
